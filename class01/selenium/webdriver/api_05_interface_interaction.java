package webdriver;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class api_05_interface_interaction {

	WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", "..\\class01\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void demoHover1() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();

		WebElement lnkHover = driver.findElement(By.xpath("//a[contains(text(),'Hover over me')]"));
		Actions action = new Actions(driver);
		action.moveToElement(lnkHover).build().perform();

		Assert.assertEquals("Hooray!", driver.findElement(By.xpath("//div[@class='tooltip-inner']")).getText());

	}
	
	@Test
	public void demoHover2() {
		driver.get("http://www.myntra.com/");
		driver.manage().window().maximize();
		
		WebElement lnkHover = driver.findElement(By.xpath(
				"//div[@class='desktop-userIconsContainer']/span[@class='myntraweb-sprite desktop-iconUser sprites-user']"));
		WebElement lnkLogin = driver.findElement(By.xpath("//a[@data-reactid='488']"));
		Actions action = new Actions(driver);
		action.moveToElement(lnkHover).moveToElement(lnkLogin);
		action.click().build().perform();
		
		WebElement divLogin = driver.findElement(By.xpath("//div[@class='login-box']"));
		Assert.assertTrue(divLogin.isDisplayed());
		
	}
	
	@Test
	public void demoClickAndHold() {
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		driver.manage().window().maximize();
		
		List<WebElement> eHold = driver.findElements(By.xpath(
				"//ol[@id='selectable']//li"));
		Actions action = new Actions(driver);
		action.clickAndHold(eHold.get(0)).clickAndHold(eHold.get(3)).click().perform();
		
		int size = driver.findElements(By.xpath("//li[@class='ui-state-default ui-selectee ui-selected']")).size();
		Assert.assertEquals(4, size);
		
	}
	
	@Test
	public void demoDoubleClick() {
		driver.get("http://www.seleniumlearn.com/double-click");
		driver.manage().window().maximize();
		
		WebElement eDouble = driver.findElement(By.xpath(
				"//button[contains(text(),'Double-Click Me!')]"));
		Actions action = new Actions(driver);
		action.doubleClick(eDouble).build().perform();
		
		Alert alert = driver.switchTo().alert();
		Assert.assertEquals("The Button was double-clicked.", alert.getText());
		alert.accept();
		
	}
	
	@Test
	public void demoRightClick() {
		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");
		driver.manage().window().maximize();
		
		WebElement eRight = driver.findElement(By.xpath(
				"//span[contains(text(), 'right click me')]"));
		Actions action = new Actions(driver);
		action.contextClick(eRight);
		action.moveToElement(driver.findElement(By.xpath("//li[contains(@class,'context-menu-icon-quit')]"))).perform();
		
		WebElement eQuit = driver.findElement(By.xpath("//li[contains(@class,'context-menu-visible') and contains(.,'Quit')]"));
		Assert.assertTrue(eQuit.isDisplayed());
		
		action.click().build().perform();
		
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
	}
	
	
	@Test
	public void demoDragAndDrop1() {
		driver.get("http://demos.telerik.com/kendo-ui/dragdrop/angular");
		driver.manage().window().maximize();
		
		WebElement eSource = driver.findElement(By.xpath(
				"//div[@id='draggable']"));
		WebElement eTarget = driver.findElement(By.xpath(
				"//div[@id='droptarget']"));
		
		Actions action = new Actions(driver);
		action.dragAndDrop(eSource, eTarget).build().perform();;
		
		Assert.assertEquals("You did great!", eTarget.getText());
		
	}
	
	@Test
	public void demoDragAndDrop2() {
		driver.get("http://jqueryui.com/resources/demos/droppable/default.html");
		driver.manage().window().maximize();

		WebElement eSource = driver.findElement(By.xpath(
				"//p[contains(text(),'Drag me to my target')]"));
		WebElement eTarget = driver.findElement(By.xpath(
				"//div[@id='droppable']"));
		
		Actions action = new Actions(driver);
		action.dragAndDrop(eSource, eTarget).build().perform();;

		Assert.assertEquals("Dropped!", eTarget.getText());

	}
	
	

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
