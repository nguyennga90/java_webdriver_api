package webdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class api_09_webdriver_wait {
	WebDriver driver;

	@Test
	public void testScript01() {
		driver = new FirefoxDriver();

		// Step01
		driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
		driver.manage().window().maximize();
		// Step02
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Step03
		driver.findElement(By.xpath("//div[@id='start']/button")).click();
		// Step04,5
		Assert.assertEquals("Hello World!", driver.findElement(By.xpath("//div[@id='finish']/h4")).getText());
	}

	@Test
	public void testScript02() {
		driver = new FirefoxDriver();

		// Step01
		driver.get(
				"http://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		WebDriverWait wait = new WebDriverWait(driver, 15);

		// Step02
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("calendarContainer")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("calendarContainer")));

		// Step03
		Assert.assertEquals("No Selected Dates to display.",
				driver.findElement(By.xpath("//span[@id='ctl00_ContentPlaceholder1_Label1']")).getText().trim());

		// Step04
		driver.findElement(By.xpath("//td[a[contains(text(), '12')]]")).click();

		// Step05
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".raDiv")));

		// Step06
		wait.until(ExpectedConditions
				.visibilityOf(driver.findElement(By.xpath("//*[contains(@class,'rcSelected')]//a[text()='12']"))));

		// Step07
		Assert.assertEquals("Tuesday, December 12, 2017",
				driver.findElement(By.xpath("//span[@id='ctl00_ContentPlaceholder1_Label1']")).getText().trim());
	}

	@Test
	public void testScript03() {
		driver = new FirefoxDriver();

		// Step01
		driver.get("https://stuntcoders.com/snippets/javascript-countdown/");
		driver.manage().window().maximize();

		WebDriverWait wait = new WebDriverWait(driver, 15);

		// Step02
		WebElement countdown = driver.findElement(By.xpath("//div[@id='javascript_countdown_time']"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='javascript_countdown_time']")));

		// Step03
		new FluentWait<WebElement>(countdown).withTimeout(10, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
				.until(new Function<WebElement, Boolean>() {
					public Boolean apply(WebElement element) {
						boolean isEnd = element.getText().endsWith("02");
						return isEnd;
					}
				});
	}

	@Test
	public void testScript04() {
		driver = new FirefoxDriver();

		// Step01
		driver.get("http://toolsqa.wpengine.com/automation-practice-switch-windows/");
		driver.manage().window().maximize();

		WebElement clock = driver.findElement(By.xpath("//span[@id='clock']"));

		// Step02
		new FluentWait<WebElement>(clock).withTimeout(40, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class).until(new Function<WebElement, Boolean>() {
					public Boolean apply(WebElement element) {
						element = driver.findElement(By.xpath("//*[@style='color: red;']"));
						return element.isDisplayed();
					}
				});

		new FluentWait<WebElement>(clock).withTimeout(40, TimeUnit.SECONDS).pollingEvery(10, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class).until(new Function<WebElement, Boolean>() {
					public Boolean apply(WebElement element) {
						element = driver
								.findElement(By.xpath("//span[@id='clock' and contains(text(), 'Buzz  Buzz')]"));
						return element.isDisplayed();
					}
				});

	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
