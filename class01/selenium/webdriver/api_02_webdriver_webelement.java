package webdriver;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class api_02_webdriver_webelement {

	WebDriver driver;
	
	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@Test
	public void TC_01_IsDisplayed() {
		WebElement weEmail = driver.findElement(By.xpath("//input[@id='mail']"));
		if (weEmail.isDisplayed()) {
			weEmail.sendKeys("Automation Testing");
		}
		WebElement weAgeUnder18 = driver.findElement(By.xpath("//input[@id='under_18']"));
		if (weAgeUnder18.isDisplayed()) {
			weAgeUnder18.click();
		}
		WebElement weEdu = driver.findElement(By.xpath("//textarea[@id='edu']"));
		if (weEdu.isDisplayed()) {
			weEdu.sendKeys("Automation Testing");
		}
	}
	
	@Test
	public void TC_02_IsEnabled() {
		WebElement weEmail = driver.findElement(By.xpath("//input[@id='mail']"));
		if (weEmail.isEnabled()) {
			System.out.println("Email element is enable");
		}
		WebElement weAgeUnder18 = driver.findElement(By.xpath("//input[@id='under_18']"));
		if (weAgeUnder18.isEnabled()) {
			System.out.println("Age under 18 element is enable");
		}
		WebElement weEdu = driver.findElement(By.xpath("//textarea[@id='edu']"));
		if (weEdu.isEnabled()) {
			System.out.println("Education element is enable");
		}
		WebElement weJobRole01 = driver.findElement(By.xpath("//select[@id='job1']"));
		if (weJobRole01.isEnabled()) {
			System.out.println("Job Role 01 element is enable");
		}
		WebElement weDevelopment = driver.findElement(By.xpath("//input[@id='development']"));
		if (weDevelopment.isEnabled()) {
			System.out.println("Interests (Development) element is enable");
		}
		WebElement weSlider01 = driver.findElement(By.xpath("//input[@id='slider-1']"));
		if (weSlider01.isEnabled()) {
			System.out.println("Slider 01 element is enable");
		}
		WebElement weEnableButton = driver.findElement(By.xpath("//button[@id='button-enabled']"));
		if (weEnableButton.isEnabled()) {
			System.out.println("Enable Button element is enable");
		}
		WebElement wePass = driver.findElement(By.xpath("//input[@id='password']"));
		if (!wePass.isEnabled()) {
			System.out.println("Pass element is disable");
		}
		WebElement weDisableRadio = driver.findElement(By.xpath("//input[@id='radio-disabled']"));
		if (!weDisableRadio.isEnabled()) {
			System.out.println("Disable radio element is disable");
		}
		WebElement weBio = driver.findElement(By.xpath("//textarea[@id='bio']"));
		if (!weBio.isEnabled()) {
			System.out.println("Biography element is disable");
		}
		WebElement weJobRole02 = driver.findElement(By.xpath("//select[@id='job2']"));
		if (!weJobRole02.isEnabled()) {
			System.out.println("Job Role 02 element is disable");
		}
		WebElement weDisableCheckbox = driver.findElement(By.xpath("//input[@id='check-disbaled']"));
		if (!weDisableCheckbox.isEnabled()) {
			System.out.println("Disable checkbox element is disable");
		}
		WebElement weSlider02 = driver.findElement(By.xpath("//input[@id='slider-2']"));
		if (!weSlider02.isEnabled()) {
			System.out.println("Slider 02 element is disable");
		}
		WebElement weDisableButton = driver.findElement(By.xpath("//button[@id='button-disabled']"));
		if (!weDisableButton.isEnabled()) {
			System.out.println("Disable button element is disable");
		}
	}
	
	@Test
	public void TC_03_IsSelected() {
		WebElement weAgeUnder18 = driver.findElement(By.xpath("//input[@id='under_18']"));
		weAgeUnder18.click();
		if (!weAgeUnder18.isSelected()) {
			weAgeUnder18.click();
		}
		WebElement weDevelopment = driver.findElement(By.xpath("//input[@id='development']"));
		weDevelopment.click();
		if (!weDevelopment.isSelected()) {
			weDevelopment.click();
		}
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
