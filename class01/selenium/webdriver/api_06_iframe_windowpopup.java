package webdriver;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class api_06_iframe_windowpopup {

	WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {
		driver = new FirefoxDriver();
		// System.setProperty("webdriver.chrome.driver",
		// "..\\class01\\driver\\chromedriver.exe");
		// driver = new ChromeDriver();
	}

	// @Test
	public void testScript01() {
		driver.get("http://www.hdfcbank.com/");
		driver.manage().window().maximize();

		WebElement weLooingForIFrame = driver.findElement(By.xpath("//div[@class='flipBannerWrap']//iframe"));
		driver.switchTo().frame(weLooingForIFrame);

		WebElement weLooingForSpan = driver.findElement(By.xpath("//*[@id='messageText']"));

		Assert.assertEquals("What are you looking for?", weLooingForSpan.getText());

		driver.switchTo().defaultContent();

		WebElement weImageIFrame = driver.findElement(By.xpath("//div[@class='slidingbanners']//iframe"));
		driver.switchTo().frame(weImageIFrame);

		List<WebElement> lEImage = driver
				.findElements(By.xpath("//div[@id='productcontainer']//img[@class='bannerimage']"));

		Assert.assertEquals(6, lEImage.size());

		driver.switchTo().defaultContent();

		WebElement weFlipBanner = driver.findElement(By.xpath("//div[@class='flipBanner']"));
		Assert.assertTrue(weFlipBanner.isDisplayed());

	}

	// @Test
	public void testScript02() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();

		String parent = driver.getWindowHandle();

		driver.findElement(By.xpath("//a[contains(text(),'Click Here')]")).click();

		switchToWindowByID(parent);

		Assert.assertEquals("Google", driver.getTitle());

		closeAllWindowsWithoutParent(parent);
	}

	@Test
	public void testScript03() {
		driver.get("http://www.hdfcbank.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// WebElement weAd =
		// driver.findElement(By.xpath("//img[@alt='banner']"));

		Actions action = new Actions(driver);

		// if (weAd.isDisplayed()) {
		// action.moveToElement(driver.findElement(By.xpath("//div[@class='cloase2']//a"))).click().build().perform();
		// }

		String parent = driver.getWindowHandle();

		WebDriverWait wait = new WebDriverWait(driver, 20);

		WebElement weLnkArgi = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[@class='sectionnav']//a[contains(text(),'Agri')]")));
		action.moveToElement(weLnkArgi).click().build().perform();

		switchToWindowByID(parent);

		WebElement weAccDetail = driver.findElement(By.xpath("//p[contains(text(),'Account Details')]"));
		action.moveToElement(weAccDetail).click().build().perform();
		switchToWindowByTitle("Welcome to HDFC Bank NetBanking");

		WebElement weFramePolicy = driver.findElement(By.xpath("//frame[@name='footer']"));
		driver.switchTo().frame(weFramePolicy);

		WebElement wePolicy = driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]"));
		action.moveToElement(wePolicy).click().build().perform();
		switchToWindowByTitle(
				"HDFC Bank - Leading Bank in India, Banking Services, Private Banking, Personal Loan, Car Loan");

		WebElement weCSR = driver.findElement(By.xpath("//a[contains(text(),'CSR')]"));
		action.moveToElement(weCSR).click().build().perform();

		closeAllWindowsWithoutParent(parent);
	}

	public void switchToWindowByID(String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String childWindows : allWindows) {
			if (!childWindows.equals(parent)) {
				driver.switchTo().window(childWindows);
				break;
			}
		}
	}

	public void switchToWindowByTitle(String title) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String childWindows : allWindows) {
			driver.switchTo().window(childWindows);
			String childTitle = driver.getTitle();
			if (childTitle.equals(title)) {
				break;
			}
		}
	}

	public boolean closeAllWindowsWithoutParent(String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String childWindows : allWindows) {
			if (!childWindows.equals(parent)) {
				driver.switchTo().window(childWindows);
				driver.close();
			}
		}
		driver.switchTo().window(parent);

		// Assert window = 1
		if (driver.getWindowHandles().size() == 1)
			return true;
		else
			return false;
	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
