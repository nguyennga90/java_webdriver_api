package webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class api_07_javascript_executor {

	WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.ie.driver", "..\\class01\\driver\\IEDriverServer.exe");
		driver = new InternetExplorerDriver();
	}

	@Test
	public void TC01() throws Exception {
		//Step01
		driver.get("http://live.guru99.com/");
		driver.manage().window().maximize();
		
		// Step02
		String strDomain = (String) handleWithBrowser(driver, "return document.domain;");
		Assert.assertEquals("live.guru99.com", strDomain);

		// Step03
		String strURL = handleWithBrowser(driver, "return document.URL;").toString();
		Assert.assertEquals("http://live.guru99.com/", strURL);

		// Step04
		WebElement eMobile = driver.findElement(By.xpath("//a[contains(text(),'Mobile')]"));
		clickElement(driver, eMobile);
		Thread.sleep(5000);

		// Step05
		WebElement eSamSung = driver.findElement(By.xpath(
				"//h2[a[contains(text(), 'Samsung Galaxy')]]/following-sibling::div[@class='actions']//span[contains(text(), 'Add to Cart')]"));
		clickElement(driver, eSamSung);
		Thread.sleep(5000);

		// Step06
		String txtSamSung = handleWithBrowser(driver, "return document.documentElement.innerText;").toString();
		Assert.assertTrue(txtSamSung.contains("Samsung Galaxy was added to your shopping cart."));

		// Step07
		WebElement ePrivacy = driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]"));
		clickElement(driver, ePrivacy);
		Thread.sleep(5000);

		String strTitle = handleWithBrowser(driver, "return document.title;").toString();
		Assert.assertEquals("Privacy Policy", strTitle);

		// Step08
		handleWithBrowser(driver, "window.scrollBy(0,document.body.scrollHeight)");

		// Step09
		WebElement eTd = driver.findElement(By.xpath(
				"//th[contains(text(), 'WISHLIST_CNT')]/following-sibling::td[contains(text(), 'The number of items in your Wishlist.')]"));
		Assert.assertTrue(eTd.isDisplayed());

		// Step10
		handleWithBrowser(driver, "window.location = 'http://demo.guru99.com/v4/'");
		Thread.sleep(4000);

		String strDomainNavigate = handleWithBrowser(driver, "return document.domain;").toString();
		Assert.assertEquals("demo.guru99.com", strDomainNavigate);

	}
	
	@Test
	public void TC02() throws Exception {
		//Step01
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_disabled");
		driver.manage().window().maximize();
		
		//Step02
		WebElement eIFrameResult = driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		driver.switchTo().frame(eIFrameResult);
		
		WebElement eLastName = driver.findElement(By.xpath("//input[@name='lname']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].removeAttribute('disabled','false');", eLastName);
		
		//Step03
		eLastName.sendKeys("Automation Testing");
		
		//Step04
		WebElement eSubmit = driver.findElement(By.xpath("//input[@type='submit']"));
		clickElement(driver, eSubmit);
		Thread.sleep(4000);
		
		//Step05
		String txtAutoTest = handleWithBrowser(driver, "return document.documentElement.innerText;").toString();
//		System.out.println(txtAutoTest);
		Assert.assertTrue(txtAutoTest.contains("Automation Testing"));

	}

	public void clickElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
	}

	public Object handleWithBrowser(WebDriver driver, String str) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Object objReturn = js.executeScript(str);
		return objReturn;
	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
