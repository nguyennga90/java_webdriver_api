package webdriver;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class api_08_upload {

	WebDriver driver;
	String strCG = "C:\\chaoga.jpg";
	String strCGName = "chaoga.jpg";

	@Test
	public void testScript01() throws Exception {
		driver = new FirefoxDriver();

		driver.get("http://www.helloselenium.com/2015/03/how-to-upload-file-using-sendkeys.html");
		driver.manage().window().maximize();

		WebElement btnUpload = driver.findElement(By.xpath("//input[@name='uploadFileInput']"));
		btnUpload.sendKeys(strCG);

		Thread.sleep(3000);
	}

	@Test
	public void testScript02FireFox() throws Exception {
		driver = new FirefoxDriver();

		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().window().maximize();

		WebElement upload = driver.findElement(By.xpath("//input[@name='files[]']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", upload);

		Runtime.getRuntime().exec(new String[] { "..\\class01\\upload\\firefox.exe", strCG });
		Thread.sleep(3000);

		Assert.assertTrue(driver.findElement(By.xpath("//p[@class='name' and contains(text(),'" + strCGName + "')]"))
				.isDisplayed());
	}

	@Test
	public void testScript02IE() throws Exception {
		System.setProperty("webdriver.ie.driver", "..\\class01\\driver\\IEDriverServer.exe");
		driver = new InternetExplorerDriver();

		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().window().maximize();

		WebElement upload = driver.findElement(By.xpath("//span[contains(text(),'Add files...')]"));
		upload.click();

		Runtime.getRuntime().exec(new String[] { "..\\class01\\upload\\ie.exe", strCG });
		Thread.sleep(3000);

		Assert.assertTrue(driver.findElement(By.xpath("//p[@class='name' and contains(text(),'" + strCGName + "')]"))
				.isDisplayed());
	}

	@Test
	public void testScript02Chrome() throws Exception {
		System.setProperty("webdriver.chrome.driver", "..\\class01\\driver\\chromedriver.exe");
		driver = new ChromeDriver();

		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().window().maximize();

		WebElement upload = driver.findElement(By.cssSelector(".fileinput-button"));
		upload.click();

		Runtime.getRuntime().exec(new String[] { "..\\class01\\upload\\chrome.exe", strCG });
		Thread.sleep(3000);

		Assert.assertTrue(driver.findElement(By.xpath("//p[@class='name' and contains(text(),'" + strCGName + "')]"))
				.isDisplayed());
	}

	@Test
	public void testScript03IE() throws Exception {
		System.setProperty("webdriver.ie.driver", "..\\class01\\driver\\IEDriverServer.exe");
		driver = new InternetExplorerDriver();

		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().window().maximize();

		// Specify the file location with extension
		StringSelection select = new StringSelection(strCG);

		// Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);

		// Click

		WebElement upload = driver.findElement(By.xpath("//span[contains(text(),'Add files...')]"));
		upload.click();

		Robot robot = new Robot();
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(5000);

		Assert.assertTrue(driver.findElement(By.xpath("//p[@class='name' and contains(text(),'" + strCGName + "')]"))
				.isDisplayed());
	}

	@Test
	public void testScript03Chrome() throws Exception {
		System.setProperty("webdriver.chrome.driver", "..\\class01\\driver\\chromedriver.exe");
		driver = new ChromeDriver();

		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().window().maximize();

		// Specify the file location with extension
		StringSelection select = new StringSelection(strCG);

		// Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);

		// Click
		driver.findElement(By.cssSelector(".fileinput-button")).click();

		Robot robot = new Robot();
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(5000);

		Assert.assertTrue(driver.findElement(By.xpath("//p[@class='name' and contains(text(),'" + strCGName + "')]"))
				.isDisplayed());
	}

	@Test
	public void testScript03Firefox() throws Exception {
		driver = new FirefoxDriver();

		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		driver.manage().window().maximize();

		// Specify the file location with extension
		StringSelection select = new StringSelection(strCG);

		// Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);

		// Click
		driver.findElement(By.xpath("//input[@name='files[]']")).click();

		Robot robot = new Robot();
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		Assert.assertTrue(driver.findElement(By.xpath("//p[@class='name' and contains(text(),'" + strCGName + "')]"))
				.isDisplayed());
	}

	@Test
	public void testScript04() throws Exception {
		driver = new FirefoxDriver();

		String strRandom = "ngantt" + randomInt();
		String strEmail = strRandom + "@gmai.com";
		String strFirstName = "Nguyen";

		// Step01
		driver.get("https://encodable.com/uploaddemo/");
		driver.manage().window().maximize();

		// Step02
		driver.findElement(By.xpath("//input[@id='uploadname1']")).sendKeys(strCG);

		// Step03
		Select sUploadTo = new Select(driver.findElement(By.xpath("//select[@name='subdir1']")));
		sUploadTo.selectByValue("/");

		// Step04
		driver.findElement(By.xpath("//input[@id='newsubdir1']")).sendKeys(strRandom);

		// Step05
		driver.findElement(By.xpath("//input[@id='formfield-email_address']")).sendKeys(strEmail);
		driver.findElement(By.xpath("//input[@id='formfield-first_name']")).sendKeys(strFirstName);

		// Step06
		driver.findElement(By.xpath("//input[@id='uploadbutton']")).click();

		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@id='fc_content']//dd[contains(text(), 'Email Address: ')]")));

		// Step07
		Assert.assertTrue(driver
				.findElement(
						By.xpath("//div[@id='fc_content']//dd[contains(text(), 'Email Address: " + strEmail + "')]"))
				.isDisplayed());
		Assert.assertTrue(driver
				.findElement(
						By.xpath("//div[@id='fc_content']//dd[contains(text(), 'First Name: " + strFirstName + "')]"))
				.isDisplayed());
		Assert.assertTrue(driver
				.findElement(
						By.xpath("//div[@id='fc_content']//dd[contains(text(), 'First Name: " + strFirstName + "')]"))
				.isDisplayed());
		Assert.assertEquals(strCGName,
				driver.findElement(By.xpath("//input[@id='fcfiledone1']")).getAttribute("value"));

		// Step08
		driver.findElement(By.xpath("//a[contains(text(),'View Uploaded Files')]")).click();

		// Step09
		driver.findElement(By.xpath("//a[contains(text(),'" + strRandom + "')]")).click();

		// Step10
		Assert.assertEquals(strCGName, driver.findElement(By.xpath("//a[contains(@id, 'fclink-')]")).getText());
	}

	public int randomInt() {
		Random random = new Random();
		int result = random.nextInt(9000);
		return result;
	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
