package webdriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class api_04_button_radio_checkbox_alert {
	WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {
		driver = new FirefoxDriver();
	}

	 @Test
	public void clickButton() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();

		driver.findElement(By.xpath("//button[@id='button-enabled']")).click();
		Assert.assertEquals("http://daominhdam.890m.com/#", driver.getCurrentUrl());

		driver.navigate().back();

		WebElement enableButton = driver.findElement(By.xpath("//button[@id='button-enabled']"));

		((JavascriptExecutor) driver).executeScript("arguments[0].click();", enableButton);
		Assert.assertEquals("http://daominhdam.890m.com/#", driver.getCurrentUrl());
	}

	@Test
	public void clickCheckbox() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/checkboxes");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement checkbox = driver.findElement(
				By.xpath("//*[@id='example']//li[label[contains(text(), 'Dual-zone air conditioning')]]/input"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", checkbox);

		Assert.assertTrue(checkbox.isSelected());

		if (checkbox.isSelected()) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", checkbox);
			Assert.assertFalse(checkbox.isSelected());
		}
	}

	 @Test
	public void clickRadioButton() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/radios");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement radio = driver
				.findElement(By.xpath("//*[@id='example']//li[label[contains(text(), '2.0 Petrol, 147kW')]]/input"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", radio);

		Assert.assertTrue(radio.isSelected());

		if (!radio.isSelected()) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", radio);
		}
	}

	 @Test
	public void jsAlert() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		driver.findElement(By.xpath("//button[contains(text(),'Click for JS Alert')]")).click();

		Alert alert = driver.switchTo().alert();
		Assert.assertEquals("I am a JS Alert", alert.getText());

		alert.accept();
		Assert.assertEquals("You clicked an alert successfully",
				driver.findElement(By.xpath("//p[@id='result']")).getText());
	}

	 @Test
	public void jsConfirm() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		driver.findElement(By.xpath("//button[contains(text(),'Click for JS Confirm')]")).click();

		Alert alert = driver.switchTo().alert();
		Assert.assertEquals("I am a JS Confirm", alert.getText());

		alert.dismiss();
		Assert.assertEquals("You clicked: Cancel", driver.findElement(By.xpath("//p[@id='result']")).getText());
	}

	 @Test
	public void jsPrompt() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		driver.findElement(By.xpath("//button[contains(text(),'Click for JS Prompt')]")).click();

		Alert alert = driver.switchTo().alert();
		Assert.assertEquals("I am a JS prompt", alert.getText());

		String msg = "ngantt";
		alert.sendKeys(msg);
		alert.accept();

		Assert.assertEquals("You entered: " + msg, driver.findElement(By.xpath("//p[@id='result']")).getText());
	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
