package webdriver;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class api_03_textbox_dropdownlist {
	WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {
		driver = new FirefoxDriver();
	}

	@Test
	public void workWithDropdownList() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();

		Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		Assert.assertFalse(dropdown.isMultiple());

		dropdown.selectByVisibleText("Automation Tester");
		Assert.assertEquals("Automation Tester", dropdown.getFirstSelectedOption().getText());

		dropdown.selectByValue("manual");
		Assert.assertEquals("Manual Tester", dropdown.getFirstSelectedOption().getText());

		dropdown.selectByIndex(3);
		Assert.assertEquals("Mobile Tester", dropdown.getFirstSelectedOption().getText());

		Assert.assertEquals(5, dropdown.getOptions().size());
	}

	@Test
	public void workWithTextBox() {
		driver.get("http://demo.guru99.com/v4");
		driver.manage().window().maximize();
		// Step02: Đăng nhập với thông tin: User = mngr94214 | Pass = Uqazedu
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys("mngr101234");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("sYgabad");
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();

		// Chọn menu New Customer
		driver.findElement(By.xpath("//a[contains(.,'New Customer')]")).click();

		// Nhập toàn bộ dữ liệu đúng
		driver.findElement(By.xpath("//input[@name='name']")).sendKeys("Dam Dao");
		driver.findElement(By.xpath("//input[@id='dob']")).sendKeys("16/10/1989");
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys("123 Address");
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys("Da Nang");
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys("Cam Le");
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys("650000");
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys("1223569854");
		driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys("testing" + randomMail() + "@gmai.com");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("sYgabad");
		driver.findElement(By.xpath("//input[@name='sub']")).click();

		// Get ra thông tin của Customer ID
		String cusID = driver
				.findElement(By.xpath(".//*[@id='customer']/tbody//td[text()='Customer ID']/following-sibling::td"))
				.getText();

		// Chọn menu Edit Customer > Nhập Customer ID
		driver.findElement(By.xpath("//a[contains(.,'Edit Customer')]")).click();
		driver.findElement(By.xpath("//input[@name='cusid']")).sendKeys(cusID);
		driver.findElement(By.xpath("//input[@name='AccSubmit']")).click();

		// Verify giá trị
		Assert.assertEquals("Dam Dao", driver.findElement(By.xpath("//input[@name='name']")).getAttribute("value"));

		String address = driver.findElement(By.xpath("//textarea[@name='addr']")).getText();
		Assert.assertEquals("123 Address", address);

		driver.findElement(By.xpath("//textarea[@name='addr']")).clear();
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys("Thanh Tri");
		driver.findElement(By.xpath("//input[@name='city']")).clear();
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys("Ha Noi");
		driver.findElement(By.xpath("//input[@name='sub']")).click();

		//
		String gAddress = driver
				.findElement(By.xpath(".//*[@id='customer']/tbody//td[text()='Address']/following-sibling::td"))
				.getText();
		Assert.assertEquals("Thanh Tri", gAddress);

		String city = driver
				.findElement(By.xpath(".//*[@id='customer']/tbody//td[text()='City']/following-sibling::td")).getText();
		Assert.assertEquals("Ha Noi", city);
	}

	public int randomMail() {
		Random r = new Random();
		int rMail = r.nextInt(10000) + 1;
		return rMail;
	}

	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
