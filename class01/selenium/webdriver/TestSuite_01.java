package webdriver;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class TestSuite_01 {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		// Pre-condition
		driver = new FirefoxDriver();
		driver.get("http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

	}

//		Step 01 - Truy cáº­p vÃ o trang: http://live.guru99.com/
//		Step 02 - Tá»›i trang Ä‘Äƒng nháº­p: http://live.guru99.com/index.php/customer/account/
//		Step 03 - Ä�á»ƒ trá»‘ng Username/ Password
//		Step 04 - Click Login button
//		Step 05 - Verify error message xuáº¥t hiá»‡n táº¡i 2 field:  This is a required field.
	@Test
	public void TC_02_LoginWithEmptyInformation() {
		driver.findElement(By.xpath("//div[@class='footer-container']//a[contains(text(),'My Account')]")).click();
		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("");
		driver.findElement(By.xpath("//*[@id='pass']")).sendKeys("");
		driver.findElement(By.xpath("//*[@id='send2']")).click();

		String errorEmail = driver.findElement(By.xpath("//*[@id='advice-required-entry-email']")).getText();
		Assert.assertEquals("This is a required field.", errorEmail);

		String errorPassword = driver.findElement(By.xpath("//*[@id='advice-required-entry-pass']")).getText();
		Assert.assertEquals("This is a required field.", errorPassword);
	}

//		Step 01 - Truy cáº­p vÃ o trang: http://live.guru99.com/
//		Step 02 - Tá»›i trang Ä‘Äƒng nháº­p: http://live.guru99.com/index.php/customer/account/
//		Step 03 - Nháº­p email invalid: 123434234@12312.123123
//		Step 04 - Click Login button
//		Step 05 - Verify error message xuáº¥t hiá»‡n:  Please enter a valid email address. For example johndoe@domain.com.
	@Test
	public void TC_03_LoginWithEmailInvalid() {
		driver.findElement(By.xpath("//div[@class='footer-container']//a[contains(text(),'My Account')]")).click();
		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("123434234@12312.123123");
		driver.findElement(By.xpath("//*[@id='pass']")).sendKeys("");
		driver.findElement(By.xpath("//*[@id='send2']")).click();

		String errorEmail = driver.findElement(By.xpath(".//*[@id='advice-validate-email-email']")).getText();
		Assert.assertEquals("Please enter a valid email address. For example johndoe@domain.com.", errorEmail);

		String errorPassword = driver.findElement(By.xpath("//*[@id='advice-required-entry-pass']")).getText();
		Assert.assertEquals("This is a required field.", errorPassword);
	}

//		Step 01 - Truy cáº­p vÃ o trang: http://live.guru99.com/
//		Step 02 - Tá»›i trang Ä‘Äƒng nháº­p: http://live.guru99.com/index.php/customer/account/
//		Step 03 - Nháº­p email correct and password incorrect: automation@gmail.com/ 123
//		Step 04 - Click Login button
//		Step 05 - Verify error message xuáº¥t hiá»‡n: Please enter 6 or more characters. Leading or trailing spaces will be ignored.
	@Test
	public void TC_04_LoginWithPasswordIncorrect() {
		driver.findElement(By.xpath("//div[@class='footer-container']//a[contains(text(),'My Account')]")).click();
		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("automation@gmail.com");
		driver.findElement(By.xpath("//*[@id='pass']")).sendKeys("123");
		driver.findElement(By.xpath("//*[@id='send2']")).click();
		
		String errorPassword = driver.findElement(By.xpath(".//*[@id='advice-validate-password-pass']")).getText();
		Assert.assertEquals("Please enter 6 or more characters without leading or trailing spaces.", errorPassword);
	}
	
	@Test
	public void TC_05_CreateAnAccount() {
		driver.findElement(By.xpath("//div[@class='footer-container']//a[contains(text(),'My Account')]")).click();
		driver.findElement(By.xpath("//a[@title='Create an Account']")).click();
		driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("Nga");
		driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("Nguyen");
		driver.findElement(By.xpath("//input[@id='email_address']")).sendKeys("nga_autotest" + randomMail() + "@gmail.com");
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("12345678");
		driver.findElement(By.xpath("//input[@id='confirmation']")).sendKeys("12345678");
		driver.findElement(By.xpath("//button[@title='Register']")).click();

		String msgSuccess = driver.findElement(By.xpath("//li[@class='success-msg']//span")).getText();
		Assert.assertEquals("Thank you for registering with Main Website Store.", msgSuccess);
		
		driver.findElement(By.xpath("//div[@class='account-cart-wrapper']//span[contains(.,'Account')]")).click();
		driver.findElement(By.xpath("//a[@title='Log Out']")).click();
	}
	
	public int randomMail() {
		Random r = new Random();
		int rMail = r.nextInt(10000) + 1;
		return rMail;
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}