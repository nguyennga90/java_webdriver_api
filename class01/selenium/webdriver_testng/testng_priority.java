package webdriver_testng;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class testng_priority {
	WebDriver driver;

	@BeforeMethod
	public void start() {
		driver = new FirefoxDriver();
	}

	@Test(priority=2)
	public void test01() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();

		Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		Assert.assertFalse(dropdown.isMultiple());

		dropdown.selectByVisibleText("Automation Tester");
		Assert.assertEquals("Automation Tester", dropdown.getFirstSelectedOption().getText());

	}

	@Test(priority=3)
	public void test02() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		Assert.assertFalse(dropdown.isMultiple());

		dropdown.selectByValue("manual");
		Assert.assertEquals("Manual Tester", dropdown.getFirstSelectedOption().getText());

	}

	@Test(priority=1)
	public void test03() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		Assert.assertFalse(dropdown.isMultiple());

		dropdown.selectByIndex(3);
		Assert.assertEquals("Mobile Tester", dropdown.getFirstSelectedOption().getText());

	}

	@Test
	public void test04() {
		driver.get("http://demo.guru99.com/v4");
		driver.manage().window().maximize();

		WebElement userId = driver.findElement(By.xpath("//input[@name='uid']"));
		Assert.assertTrue(userId.isDisplayed());

	}

	@Test
	public void test05() {
		driver.get("http://demo.guru99.com/v4");
		driver.manage().window().maximize();

		WebElement pass = driver.findElement(By.xpath("//input[@name='password']"));
		Assert.assertTrue(pass.isDisplayed());

	}


	@AfterMethod
	public void end() {
		driver.quit();
	}

}
