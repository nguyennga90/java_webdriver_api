package webdriver_testng;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class testng_parameter {
	WebDriver driver;

	@BeforeMethod
	@Parameters({ "browser" })
	public void start(String browser) {
		if(browser.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "..\\class01\\driver\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", "..\\class01\\driver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
	}
	

	@Test
	@Parameters({ "user", "pass" })
	public void test04(String user, String pass) {
		driver.get("http://demo.guru99.com/v4");
		driver.manage().window().maximize();

		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(user);;
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pass);
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();

		Assert.assertTrue(driver.findElement(By.xpath("//marquee")).isDisplayed());
	}

	@AfterMethod
	public void end() {
		driver.quit();
	}
}
