package webdriver_testng;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

public class testng_group {
	WebDriver driver;

	@BeforeGroups(groups = {"dropdown", "textbox"})
	public void start() {
		System.out.println("beforeMethod start");
		driver = new FirefoxDriver();
		System.out.println("beforemethod end");
	}

	@Test(groups = {"dropdown"})
	public void test01() {
		System.out.println("test01 start");
		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();

		Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		Assert.assertFalse(dropdown.isMultiple());

		dropdown.selectByVisibleText("Automation Tester");
		Assert.assertEquals("Automation Tester", dropdown.getFirstSelectedOption().getText());

	}

	@Test(groups = "dropdown")
	public void test02() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		Assert.assertFalse(dropdown.isMultiple());

		dropdown.selectByValue("manual");
		Assert.assertEquals("Manual Tester", dropdown.getFirstSelectedOption().getText());

	}

	@Test(groups = "dropdown")
	public void test03() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		Assert.assertFalse(dropdown.isMultiple());

		dropdown.selectByIndex(3);
		Assert.assertEquals("Mobile Tester", dropdown.getFirstSelectedOption().getText());

	}

	@Test(groups = "textbox")
	public void test04() {
		driver.get("http://demo.guru99.com/v4");
		driver.manage().window().maximize();

		WebElement userId = driver.findElement(By.xpath("//input[@name='uid']"));
		Assert.assertTrue(userId.isDisplayed());

	}

	@Test(groups = "textbox")
	public void test05() {
		driver.get("http://demo.guru99.com/v4");
		driver.manage().window().maximize();

		WebElement pass = driver.findElement(By.xpath("//input[@name='password']"));
		Assert.assertTrue(pass.isDisplayed());

	}


	@AfterGroups(groups = {"dropdown","textbox"})
	public void end() {
		driver.quit();
	}

}
